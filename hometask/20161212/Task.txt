Написать консольное приложение. Основные требования:
	1. Приложение должно генерировать XML через LINQ to XML вида, как представлено в конце документа. Обязательное требование - непрерывный запрос на создание. Никаких разрывов запроса. Файл XML сохранять на диск.
	2. Должна быть возможность в будущем подменить генератор XML на любой иной. DI. Никаких контейнеров.
	3. Приложение должно открыть созданный на шаге 1 файл и десериализовать его в созданное дерево классов. Все классы содержат имена полей как принято в c#. Никаких CATALOG в классах, только Catalog. Итоговый XML не должен отличаться от примера.
	4. Написать кастомную секцию в app.config и получать настройки через сериализацию.
	5. Дерево классов сохранить на диск под другим именем в виде XML.
	6. Настройки из конфига должны задавать имя файла для сериализации дерева классов на диск.


<CATALOG>
	<PLANT>
		<COMMON isPoison="true">Cowslip</COMMON>
		<BOTANICAL>Caltha palustris</BOTANICAL>
		<ZONE>4</ZONE>
		<LIGHT>Mostly Shady</LIGHT>
		<PRICE dimension="dollars">9.90</PRICE>
		<AVAILABILITY>10</AVAILABILITY>
	</PLANT>
	<PLANT>
		<COMMON isPoison="false">Dutchman's-Breeches</COMMON>
		<BOTANICAL>Dicentra cucullaria</BOTANICAL>
		<ZONE>3</ZONE>
		<LIGHT>Mostly Shady</LIGHT>
		<PRICE dimension="dollars">6.44</PRICE>
		<AVAILABILITY>234</AVAILABILITY>
	</PLANT>
	<PLANT>
		<COMMON isPoison="true">Ginger, Wild</COMMON>
		<BOTANICAL>Asarum canadense</BOTANICAL>
		<ZONE>3</ZONE>
		<LIGHT>Mostly Shady</LIGHT>
		<PRICE dimension="euros">9.03</PRICE>
		<AVAILABILITY>3</AVAILABILITY>
	</PLANT>
	<PLANT>
		<COMMON isPoison="false">Hepatica</COMMON>
		<BOTANICAL>Hepatica americana</BOTANICAL>
		<ZONE>4</ZONE>
		<LIGHT>Mostly Shady</LIGHT>
		<PRICE dimension="dollars">4.45</PRICE>
		<AVAILABILITY>56</AVAILABILITY>
	</PLANT>
	<PLANT>
		<COMMON isPoison="false">Liverleaf</COMMON>
		<BOTANICAL>Hepatica americana</BOTANICAL>
		<ZONE>4</ZONE>
		<LIGHT>Mostly Shady</LIGHT>
		<PRICE dimension="euros">3.99</PRICE>
		<AVAILABILITY>37</AVAILABILITY>
	</PLANT>
</CATALOG>